# A multi-period routing-inventory problem for a closed-loop supply chain considering heterogeneous fleet of vehicles #

This repository includes 20 randomly generated instances and their solutions used in a paper entitled "A multi-period routing-inventory problem for a closed-loop supply chain considering heterogeneous fleet of vehicles."


For more clarification, you can download the paper mentioned above from the following link:
LINK_LINK_LINK

Note that all indices, scalars, and parameters are defined in a way that intersted readers can relate them to the ones provided in the paper.


## Specification of instances ##

Instance 1: https://bitbucket.org/Pro_Data/instances_4/downloads/Inatance%201.txt

Instance 2: https://bitbucket.org/Pro_Data/instances_4/downloads/Inatance%201.txt

Instance 3: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%203.txt

Instance 4: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%204.txt

Instance 5: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%205.txt

Instance 6: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%206.txt

Instance 7: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%207.txt

Instance 8: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%208.txt

Instance 9: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%209.txt

Instance 10: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2010.txt

Instance 11: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2011.txt

Instance 12: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2012.txt

Instance 13: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2013.txt

Instance 14: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2014.txt

Instance 15: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2015.txt

Instance 16: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2016.txt

Instance 17: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2017.txt

Instance 18: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2018.txt

Instance 19: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2019.txt

Instance 20: https://bitbucket.org/Pro_Data/instances_4/downloads/Instance%2020.txt


## Solution of instances ##

Instance 1: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance1.gdx

Instance 2: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance2.gdx

Instance 3: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance3.gdx

Instance 4: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance4.gdx

Instance 5: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance5.gdx

Instance 6: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance6.gdx

Instance 7: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance7.gdx

Instance 8: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance8.gdx

Instance 9: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance9.gdx

Instance 10: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance10.gdx

Instance 11: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance11.gdx

Instance 12: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance12.gdx

Instance 13: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance13.gdx

Instance 14: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance14.gdx

Instance 15: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance15.gdx

Instance 16: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance16.gdx

Instance 17: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance17.gdx

Instance 18: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance18.gdx

Instance 19: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance19.gdx

Instance 20: https://bitbucket.org/Pro_Data/instances_4/downloads/SolInstance20.gdx